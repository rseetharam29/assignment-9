#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int main (){
//file open in write mode
	FILE*fp;
	char ch[100];
	fp=fopen("assignment9.txt","w");
	fprintf(fp,"UCSC is one of the leading institutes in Sri Lanka for computing studies.");
	fclose(fp);

//file open in read mode
	fp=fopen("assignment9.txt","r");
	printf("%s",fgets(ch,100,fp));
	fclose(fp);

//file open in append mode
	fp=fopen("assignment9.txt","a");
	fprintf(fp,"\nUCSC offers undergraduate and postgraduate level courses aiming a range of computing fields.");
	fclose(fp);

return 0;
}
